var term = require( 'terminal-kit' ).terminal ;
term.windowTitle( "Wet Game - Lee Nattress" );

const readline = require('readline');
readline.emitKeypressEvents(process.stdin);
process.stdin.setRawMode(true);

var debris = [];
function make_debris(x,y) {
  d = {}
  d.x = x;
  d.y = y;
  d.sprite = '💧';
  d.x_velocity = 0;
  d.y_velocity = 1;
  debris.push(d);
}


var movespeed = 1;
var state = start();
process.stdin.on('keypress', (str, key) => {
  if (key.ctrl && key.name === 'c') {
    console.log(`Thanks for playing!!!`);
    process.exit();
  } else {

    //keys state is all false
    state.pressedKeys = {
      left: false,
      right: false,
      up: false,
      down: false,
      space: false
    }

    //set the state of the keys pressed in the state object
    if(key.name === 'up')     { state.pressedKeys.up = true;    }
    if(key.name === 'down')   { state.pressedKeys.down = true;  }
    if(key.name === 'left')   { state.pressedKeys.left = true;  }
    if(key.name === 'right')  { state.pressedKeys.right = true; }
    if(key.name === 'space')  { state.pressedKeys.space = true; }

    // move the object based on the keys
    if(state.pressedKeys.up) { state.y = state.y - movespeed; }
    if(state.pressedKeys.down) { state.y = state.y + movespeed; }
    if(state.pressedKeys.left) { state.x = state.x - movespeed; }
    if(state.pressedKeys.right) { state.x = state.x + movespeed; }
    if(state.pressedKeys.space) {
      state = start()
    }
  }
});

function start() {
  debris = [];
  return {
    score: 0,
    active: true,
    x: 20,
    y: 20,
    pressedKeys: {
      left: false,
      right: false,
      up: false,
      down: false,
      space: false
    }
  }
}

function do_player() {
  term.moveTo( state.x*2, state.y, '☂️' );
}
function do_enemies() {
  debris.forEach((d, i)=>{ //for each enemy
    term.moveTo( d.x*2, d.y, d.sprite );
    debris[i].y = debris[i].y + d.y_velocity;
    if(d.y > 40) {
      debris.splice(i, 1);
      if(state.active) {
        state.score = state.score + 1;
      }
    }
    if(d.x === state.x && d.y === state.y){
      state.active = false;
    }
  })
}

function do_hud() {
  if(!state.active) {
    term.moveTo(10,10, 'GAME OVER! - SCORE: '+ state.score); //draw enemy sprite
  } else {
    term.moveTo(1,1,'SCORE: '+ state.score); //draw enemy sprite
  }
  term.moveTo(1,39,'ctrl + c to quit  -  Space to restart'); //draw enemy sprite
}

function draw() {
  term.clear(); //clear buffer
  if(state.active) {
    make_debris(Math.floor(Math.random() * 40),0);
    do_player() //draw player
    do_enemies() //draw debris
  }
  do_hud();
  term.moveTo( 0, 0 );
}

setInterval(draw, 33); // 33 milliseconds = ~ 30 frames per sec
